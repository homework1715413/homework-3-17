﻿#include <iostream>
using namespace std;

int main()
{
    setlocale(LC_ALL, "Rus");

    float x, y, z;

    cout << "Введите первое число \n";
    cin >> x;
    cout << "Введите второе число \n";
    cin >> y;
    cout << "Введите третье число \n";
    cin >> z;

    if (x == y && y == z) {
        cout << "Меньших чисел нет.\n";
    }

    else if (x < y && x < z) {
        x = (x + y + z) / 2;
    }

    else if (y < x && y < z) {
        y = (x + y + z) / 2;
    }

    else {
        z = (x + y + z) / 2;
    }

    cout << x << endl << y << endl << z << endl;
}